const express       = require('express');
const router        = express.Router();
const config        = require('../config.json');
const MongoClient   = require('mongodb').MongoClient;
const ObjectId      = require('mongodb').ObjectId;
const url           = require('url');

let getMongoClient = async()=>{
  return new Promise((resolve, reject)=>{
      const url = config.db.url;
      
      MongoClient.connect(url, { useNewUrlParser: true }, async(error, client)=>{
          //let mClient;
          if(error){
              console.log('MongoDB: connection error.');
              reject(new Error('mongo client error'));
          }else{
              resolve(client);
          }
      });
  });
};

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.get('/locations', async(request, response, next)=>{
  
  response.header('Content-Type', 'application/json');
  response.header('Access-Control-Allow-Origin', '*');
  response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept"); 
  response.header('Access-Control-Allow-Credentials', 'true');
  const client = await getMongoClient();
  client.db('locations').collection('locations').find({}).toArray(async(error, locations)=>{
    if(error){
      throw new Error(error);
    }else{
      response.json(locations); 
    }
  });
});

router.get('/locations/:id', async(request, response, next)=>{
  const client = await getMongoClient();
  const id = request.params.id;
  
  client.db('locations').collection('locations').find({"_id": ObjectId(id)}).toArray(async(error, location)=>{
    if(error){
      response.end(JSON.stringify(error));
    }else{
      response.writeHead(200, {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*',
        "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept"
      })
      response.json(location);
    }
  });
});

router.post('/locations', async(request, response, next)=>{
  /*
  const country = request.body.Country;
  const city = request.body.City;
  const long = request.body.location.coordinates[0];
  const lat = request.body.location.coordinates[1];
  */
  const client = await getMongoClient();
  console.log('post: ');
  let location = {
    "Country": request.body.Country,
    "City": request.body.City,
    "location": {
      "type": "Point",
      "coordinates": [ request.body.location.coordinates[0], request.body.location.coordinates[1] ]
    }
  };
  console.log(location);
  if(location){
    client.db(config.db.db).collection('locations').insert(location, (error, doc)=>{
      if(error){
        console.error(error);
        response.status(400).json({ 'ok': false, data:[{}], fails:[{'error': 'Something went wrong.'}] });
      }else{
        console.log(doc);
      }
    });
    response.status(200).json({ 'ok': true, data:[location], fails:[{}] });
  }
});

router.put('/locations/:id', async()=>{
  
});

module.exports = router;
