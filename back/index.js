const config        = require('./config.json');
const express       = require('express');
const http          = require('http');
const path          = require('path');
const logger        = require('bunyan');
const jwt           = require('jsonwebtoken');

const indexRouter   = require('./routes/index');
const authRouter    = require('./routes/auth');

const server        = express();

// view engine setup
server.set('views', path.join(__dirname, 'views'));
server.set('view engine', 'ejs');

//app.use(logger('dev'));
server.use(express.json());
server.use(express.urlencoded({ extended: false }));
server.use(express.static(path.join(__dirname, 'public')));

const authMiddleware = async(request, response, next)=>{
  //console.log(request.query.authorization);
  if(request.query.authorization){
    // let bearer = request.headers.authorization;
    // let token = bearer.split(" ")[1];
    let token = request.query.authorization;
    jwt.verify(token, "mySecret", { algorithm: "HS512" ,expiresIn: 120 }) ?
    await next() : response.json({'ok': false, fails:[{"error": "Unauthorized access."}]});
  }
};

server.use('/', authRouter);
server.use('/', authMiddleware, indexRouter);

// catch 404 and forward to error handler
server.use(function(req, res, next) {
  next(createError(404));
});

// error handler
server.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500)
  res.render('error');
});

http.createServer(server).listen(config.http.port, '127.0.0.1', ()=>{
  console.log('server listening on 127.0.0.1:3000...')
});